# mvn-hs3-2019-draft

Draft work for Hackerspace Trójmiasto sierpień opensource initivative https://hs3.pl/sierpien2019/

# Build

`mvn clean verify`

# Run

`java -jar target/mvn-hs3-2019.jar`

# Deploy on Local Apache Archiva repository

Assuming Archiva running on `localhost:8080` with `hs3` user configured

1. Export password in console  
   `export HS3PASSWORD=[password for hs3 user]`
2. Run maven build and deployment  
   `mvn -s settings.xml clean verify deploy`