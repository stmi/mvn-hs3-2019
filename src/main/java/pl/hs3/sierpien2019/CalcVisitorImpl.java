package pl.hs3.sierpien2019;

import pl.hs3.sierpien2019.CalcParser.*;

public class CalcVisitorImpl extends CalcBaseVisitor<Double> {

    @Override
    public Double visitMultiplication(MultiplicationContext ctx) {
        return this.visit(ctx.left) * this.visit(ctx.right);
    }

    @Override
    public Double visitAddition(pl.hs3.sierpien2019.CalcParser.AdditionContext ctx) {
        return this.visit(ctx.left) + this.visit(ctx.right);
    }

    @Override
    public Double visitSubtraction(SubtractionContext ctx) {
        return this.visit(ctx.left) - this.visit(ctx.right);
    }

    @Override
    public Double visitNumber(NumberContext ctx) {
        return Double.parseDouble(ctx.NUM().getText());
    }

    @Override
    public Double visitDivision(pl.hs3.sierpien2019.CalcParser.DivisionContext ctx) {
        Double right = this.visit(ctx.right);
        if (right == 0.0D) {
            throw new IllegalArgumentException("Division by 0!");
        }

        Double left = this.visit(ctx.left);

        return left / right;
    }

    @Override
    public Double visitParentheses(ParenthesesContext ctx) {
        return this.visit(ctx.inner);
    }

    @Override
    public Double visitPower(PowerContext ctx) {
        return Math.pow(this.visit(ctx.left), this.visit(ctx.right));
    }
}