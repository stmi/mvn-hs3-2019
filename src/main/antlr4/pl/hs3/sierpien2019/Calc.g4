grammar Calc;

/* Tokens (terminal) */
POW: '^';
MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';
NUM: [0-9]+;
WHITESPACE: [ \r\n\t]+ -> skip;

/* Rules (non-terminal) */
start : expr;

expr
   : NUM                          # Number
   | '(' inner=expr ')'           # Parentheses
   | left=expr op=POW right=expr  # Power
   | left=expr op=MUL right=expr  # Multiplication
   | left=expr op=DIV right=expr  # Division
   | left=expr op=ADD right=expr  # Addition
   | left=expr op=SUB right=expr  # Subtraction
   ;